package mb.home.blackjack;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class Blackjack {
	HashMap<Player,Integer> listplayers= new HashMap<>();
	public boolean add(Player player)
	{
		if(listplayers.containsKey(player))
			return false;
		else
		{
			listplayers.put(player, 0);
			return true;
		}
	}
	public boolean addCard(Player player, Integer cardValue)
	{
		if(listplayers.containsKey(player))
		{
			int value= listplayers.get(player);
			value += cardValue;
			listplayers.put(player, value);
			return true;
				
		}
		else 
			return false;
	}
	
	public Integer getValue(Player player) 
	{
		
		if(listplayers.containsKey(player))
		{
			return  listplayers.get(player);
		}
		else return null;
	}

	public String toString()
	{
		String text="";
		Set<Entry<Player,Integer>> playerList= listplayers.entrySet();
		for (Entry<Player, Integer> entry : playerList) {
			text += entry.getKey().getName() + " ,Kartenwert: " + entry.getValue()+"\n";
		}
		return text;
			
		
	}
	
}
