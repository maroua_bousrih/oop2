
import java.util.LinkedList;

import java.util.ListIterator;


import mb.home.student.Student;

public class DemoStudent {

	public static void main(String[] args) {
	
		Student s1= new Student("maroua");
		Student s2= new Student("bassem");
		Student s3= new Student("med");
		Student s4= new Student("nour");

		LinkedList<Student> list= new LinkedList<>();
		list.addFirst(s1);
		list.addFirst(s2);
		list.addFirst(s3);
		list.addFirst(s4);
		//System.out.println(list);
		ListIterator<Student> iteartor= list.listIterator();
		iteartor.next();       // P|JPM
		iteartor.next();       // PJ|PM
		iteartor.add(new Student("Marie")); // PJM|PM
		iteartor.add(new Student("Nena")); // PJMN|PM
		iteartor.next(); 		// PJMNP|M
		iteartor.remove();	// PJMN|M
	
		
		iteartor = list.listIterator();
		while(iteartor.hasNext())
		{
			System.out.println(iteartor.next());
		}

		
		
	

	}
}
