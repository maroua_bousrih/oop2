import mb.home.event.Event;
import mb.home.event.EventKalendar;

public class Demo {

	public static void main(String[] args) {
		Event ev1 = new Event("java formation","Wifi",80.55);
		Event ev4 = new Event("web formation","Wifi",100.00);
		Event ev2 = new Event("c formation","TU",150.2);
		Event ev3 = new Event("c# formation","TU",250.2);
		EventKalendar kalender= new EventKalendar();
		kalender.add(ev1);
		kalender.add(ev2);
		kalender.add(ev3);
		kalender.add(ev4);
		System.out.println(kalender);
		System.out.println(kalender.getByTitle("c formation"));
		System.out.println(kalender.getByOrt("TU"));
		System.out.println("***********");
		System.out.println(kalender.getByEintrittsPreis(150, 250.0));
		System.out.println(kalender.getMostExpensiveByOrt("TU"));
		System.out.println(kalender.getAvgPreisByOrt("Wifi"));
		System.out.println(kalender.getCountEventsByOrt());
		System.out.println(kalender.getSumPriceEventsByOrt());
	}

}
