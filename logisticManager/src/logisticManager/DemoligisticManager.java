package logisticManager;

public class DemoligisticManager {

	public static void main(String[] args) {
		Car car1= new Car("BMW", "black", 200000);
		Car car2= new Car("Polo", "blue", 200000);
		
		Shirt shirt1= new Shirt("zara", "M", "green");
		Shirt shirt2= new Shirt("zara", "L", "green");
		
		LogisticManager logistic= new LogisticManager();
		logistic.addProduct(car1);
		logistic.addProduct(car2);
		logistic.addProduct(shirt1);
		logistic.addProduct(shirt2);
		logistic.moveall("Wien");
		System.out.println(logistic);
		
	}
	
	
	

}
