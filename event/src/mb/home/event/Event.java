package mb.home.event;

public class Event {
	private String Title, ort;
	private double Eintrittspreis;
	public Event(String title, String ort, double eintrittspreis) {
		super();
		Title = title;
		this.ort = ort;
		Eintrittspreis = eintrittspreis;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public void setOrt(String ort) {
		this.ort = ort;
	}
	public void setEintrittspreis(double eintrittspreis) {
		Eintrittspreis = eintrittspreis;
	}
	public String getTitle() {
		return Title;
	}
	public String getOrt() {
		return ort;
	}
	public double getEintrittspreis() {
		return Eintrittspreis;
	}
	@Override
	public String toString() {
		return "Event [Title=" + Title + ", ort=" + ort + ", Eintrittspreis=" + Eintrittspreis + "]";
	}
	
	
	
	

}
