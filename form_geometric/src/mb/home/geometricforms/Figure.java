package mb.home.geometricforms;

public interface Figure {

	public double getPerimeter();
	public double getArea();
}
