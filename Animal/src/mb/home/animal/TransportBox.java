package mb.home.animal;

public class TransportBox<T> extends Box {

	private String color;
	private T content;
	public TransportBox(String color,int length, int width, int heigh, T content) {
		super(length, width, heigh);
		this.color=color;
		this.content=content;
		
	}
	public String getColor() {
		return color;
	}

	public T getContent() {
		return content;
	}
	
	public int calculVolume()
	{
		return (int)(heigh * width * length * 0.7);
	}
	
		

}
