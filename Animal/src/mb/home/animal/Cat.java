/**
 * 
 */
package mb.home.animal;

/**
 * @author basse
 *
 */
public class Cat extends Animal {

	/**
	 * @param name
	 * @param color
	 * @param lovely
	 */
	private boolean lovely;
	private int weight; 
	public Cat(String name, String color, boolean lovely, int weight) {
		super(name, color);
		this.lovely=lovely;
		this.weight=weight;
	}

	/* (non-Javadoc)
	 * @see mb.home.animal.Animal#makeNoise()
	 */
	@Override
	public void makeNoise() {
		System.out.println("Miau miauuuuuu");

	}
	
	/* (non-Javadoc)
	 * @see mb.home.animal.Animal#toString()
	 */
	@Override
	public String toString() {
		return "Cat [lovely=" + lovely + ", color=" + color + ", name= " + name +", weight=" + weight +"]";
	}

	/* (non-Javadoc)
	 * @see mb.home.animal.Animal#setName(java.lang.String)
	 */
	
	public void setName(String name) {
		this.name=name;
	}
	@Override
	public Integer getWeight() {
		return weight;
	}
	@Override
	public String getName()
	{
		return this.name;
	}

}
