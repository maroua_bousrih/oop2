/**
 * 
 */
package mb.home.animal;

/**
 * @author basse
 *
 */
public class Dog extends Animal {

	/**
	 * @param name
	 * @param color
	 * @param weight
	 */
	private int weight; 
	public Dog(String name, String color, int weight) {
		super(name, color);
		this.weight=weight;
	}
	

	/* (non-Javadoc)
	 * @see mb.home.animal.Animal#makeNoise()
	 */
	@Override
	public void makeNoise() 
	{
		System.out.println(name + " says"+" wuff, wufffff");
	}


	@Override
	public String toString() {
		return "Dog [weight=" + weight + ", color=" + color + ", name=" + name + "]";
	}

	@Override
	public Integer getWeight() {
		return weight;
	}


	public void setWeight(int weight) {
		this.weight = weight;
	}
	
	public void setName(String name) 
	{
		this.name=name;
	}
	@Override
	public String getName()
	{
		return this.name;
	}
	

}
