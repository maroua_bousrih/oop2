import mb.home.emp.Employee;
import mb.home.emp.EmployeeManager;

public class DemoEmp {

	public static void main(String[] args) {
		EmployeeManager firma = new EmployeeManager();
		Employee emp1 = new Employee(1, "bassem", 2850.40, "Firmware");
		Employee emp2 = new Employee(2, "maroua", 2050.40, "Firmware");
		
		firma.addEmployee(emp1);
		firma.addEmployee(emp2);
		
		System.out.println(firma.findByMaxSalary());
		System.out.println(firma.findByEmpNumber(2));
		System.out.println(firma.findByDepartment("Firmware"));
		
		
	}

}
