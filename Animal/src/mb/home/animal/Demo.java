/**
 * 
 */
package mb.home.animal;

import java.util.HashMap;

/**
 * @author Maroua
 *
 */
public class Demo {

	public static void main(String[] args) 
	{
		Dog dog= new Dog("rafli", "black", 4000);
		Cat cat= new Cat("susi","white",true,1500);
		Frog frog = new Frog("fikhi", "green", "yellow",500);
		Dog dog1= new Dog("raffi", "black", 5000);
		Cat cat1= new Cat("ddf","white",true,2000);
		Frog frog1 = new Frog("fgtf", "green", "yellow",750);
		Dog dog2= new Dog("hipk", "black", 10000);
		
		
		ZooManager zooGraz= new ZooManager();
		
		zooGraz.addAnimal(dog);
		zooGraz.addAnimal(cat);
		zooGraz.addAnimal(frog);
		zooGraz.addAnimal(dog1);
		zooGraz.addAnimal(dog2);
		zooGraz.addAnimal(cat1);
		zooGraz.addAnimal(frog1);
		
		System.out.println(zooGraz);
		
		System.out.println(zooGraz.accountAnimalbyColor());
		System.out.println(zooGraz.accountAnimalbyBreed());
		System.out.println(zooGraz.sumweightbybyBreed());
		System.out.println(zooGraz.namebyAnimal("cat"));
		System.out.println(zooGraz.biggestAnimalByType("FroG"));
		
		
		
	}
	

	


}
