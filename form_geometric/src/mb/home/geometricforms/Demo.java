package mb.home.geometricforms;

public class Demo {

	
	public static void main(String[] args) {
		
		Rectangle rec1= new Rectangle(100, 15.2);
		System.out.println(rec1.getPerimeter()+" // "+ rec1.getArea());
		
		Rectangle rec2= new Rectangle(103.4, 14);
		System.out.println(rec2.getPerimeter()+" // "+ rec2.getArea());
		Circle cir1= new Circle(50);
		System.out.println(cir1.getPerimeter()+" // "+ cir1.getArea());
		Circle cir2= new Circle(8);
		System.out.println(cir2.getPerimeter()+" // "+ cir2.getArea());
		
		FigureManager listFigure= new FigureManager();
		listFigure.add(rec1);
		listFigure.add(rec2);
		listFigure.add(cir1);
		listFigure.add(cir2);
		System.out.println("////////");
		System.out.println(listFigure.getMaxPerimeter());
		System.out.println(listFigure.getAverageAreaSize());
		System.out.println(listFigure.getAreaBySizeCategories());
	}

}
