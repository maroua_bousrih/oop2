package mb.home.emp;

import java.util.ArrayList;


public class EmployeeManager {
	ArrayList<Employee> listEmployee= new ArrayList<Employee>();
	
	public void addEmployee(Employee employee)
	{
		listEmployee.add(employee);
	}
	public Employee findByMaxSalary()
	{
		Employee max=listEmployee.get(0);
		for (Employee employee : listEmployee) 
		{
			if(employee.getSalary()>max.getSalary())
			{
				max = employee;
			}
		}
		return max;
	}
	public Employee findByEmpNumber(int number)
	{
		Employee result=null;
		if(listEmployee.isEmpty())
			return null;
		
		for (Employee employee : listEmployee) {
			if(employee.getEmpNumber()== number)
				result= employee;
		}
		return result;
		
	}
	public ArrayList<Employee> findByDepartment(String department) 
	{
		
		
		ArrayList<Employee> DepEmplo= new ArrayList<Employee>();
		for (Employee employee :listEmployee) {
			if(employee.getDepartment()==department )
			{
				DepEmplo.add(employee);
			}
			
		}
		return DepEmplo;
	}
	
}
