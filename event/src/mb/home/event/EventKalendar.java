package mb.home.event;

import java.util.ArrayList;
import java.util.HashMap;

public class EventKalendar {

	private ArrayList<Event> listEvent=new ArrayList<>();
	
	public void add(Event e)
	{
		listEvent.add(e);
	}
	
	public Event getByTitle(String title) 
	{
		Event result=null;
		for (Event event : listEvent) {
			if (event.getTitle()==title)
				result = event;	
		}
		return result;
		
	}

	public ArrayList<Event> getByOrt(String ort) 
	{
		ArrayList<Event> result= new ArrayList<>();
		for (Event event : listEvent) {
			if(event.getOrt()==ort)
				result.add(event);
			
		}
		return result;
	}
	public ArrayList<Event> getByEintrittsPreis(double min, double max) 
	{
		ArrayList<Event> result= new ArrayList<>();
		for (Event event : listEvent) {
			if(event.getEintrittspreis() >= min && event.getEintrittspreis() <= max)
				result.add(event);
			
		}
		return result;
	}
	
	public ArrayList<Event> getMostExpensiveByOrt(String ort) 
	{
		ArrayList<Event> eventbyOrt = getByOrt(ort);
		Event max= eventbyOrt.get(0);
		ArrayList<Event> result= new ArrayList<>();
		for (Event event : eventbyOrt) {
			if(event.getEintrittspreis()>max.getEintrittspreis())
			{
				result.add(event);
			}
			
		}
		return result;
	}
	public double getAvgPreisByOrt(String ort) 
	{
		double result=0;
		ArrayList<Event> evenbyOrt= getByOrt(ort);
		for (Event event : evenbyOrt)
		{
			result= result + event.getEintrittspreis();
		}
		return result/evenbyOrt.size();
	}
	public HashMap<String, Integer> getCountEventsByOrt() 
	{
		HashMap<String, Integer> result= new HashMap<>();
		for (Event event : listEvent) {
			if(result.containsKey(event.getOrt()))
			{
				Integer count= result.get(event.getOrt());
				count ++;
				result.put(event.getOrt(), count);
			}
			else
				result.put(event.getOrt(), 1);
		}
		
		return result;
		
	}
	public HashMap<String, Double> getSumPriceEventsByOrt() 
	{
		HashMap<String, Double> result= new HashMap<>();
		for (Event event : listEvent) {
			if(result.containsKey(event.getOrt()))
			{
				Double sum= result.get(event.getOrt());
				sum = sum + event.getEintrittspreis() ;
				result.put(event.getOrt(), sum);
				
			}
			else 
				result.put(event.getOrt(), event.getEintrittspreis());
			
		}
		return result;
	}
	@Override
	public String toString() {
		return "EventKalendar [listEvent=" + listEvent + "]";
	}
	
}
