package mb.home.animal;

public class Frog extends Animal {

	private String eyeColor;
	private int weight; 
	public Frog(String name, String color, String eyeColor, int weight) {
		super(name, color);
		this.eyeColor=eyeColor;
		this.weight=weight;
	}

	@Override
	public void makeNoise() {
		System.out.println("frog, frog");

	}
	
	@Override
	public Integer getWeight() {
		return weight;
	}

	@Override
	public String toString() {
		return "Frog [name= "+ name + ", eyeColor=" + eyeColor + ", color=" + color+", weight=" + weight + "]";
	}

	
	public void setName(String name) {
		this.name=name;
	}

	@Override
	public String getName()
	{
		return this.name;
	}
}
