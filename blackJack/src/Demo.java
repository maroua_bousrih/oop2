import javax.swing.text.PlainDocument;

import mb.home.blackjack.Blackjack;
import mb.home.blackjack.Player;

public class Demo {

	public static void main(String[] args) {
	
		Player p1= new Player("maroua",30);
		Player p2= new Player("bassem",36);
		
		Blackjack game= new Blackjack();
		game.add(p1);
		game.add(p2);
		System.out.println(game);
		game.addCard(p1, 10);
		game.addCard(p1, 20);
		game.addCard(p2, 10);
		System.out.println(game);

	}

}
