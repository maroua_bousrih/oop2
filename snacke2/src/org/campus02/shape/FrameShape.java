package org.campus02.shape;

public class FrameShape extends Shape {

    protected int x;
    protected int y;
    public FrameShape() 
    {
         
        super();
        
        position(5,5);
        this.x=5;
        this.y=5;
    }
    public int getX() 
    {
        return x;
    }
    public void setXY(int x, int y) 
    {
        array[getX()][getY()]=' ';
        position(x, y);
        setx(x);
        setY(y);
    }
    public int getY()
    {
        return y;
    }
    public void setY(int y)
    {
        this.y = y;
    }
    public void setx(int x) 
    {
        this.x = x;
    }
    
     public void position(int x, int y)
     {
         for(int index = 0 ; index<array.length; index++)
         {
            for(int index2=0; index2<array.length; index2++)
            {
            	if(index2==0 || index == 0 || index==array.length-1 || index2== array.length-1)
                {
                    array[index][index2]='X';
                }
            }
         }
         array[x][y]='S';
     }
 
	 public void moveUp() 
	 {
	     if(!(array[x-1][y]=='X'))
	     {
	        setXY(x-1, y);
	     }
	  }
	 public void moveDown() 
	 {
	     if(!(array[x+1][y]=='X'))
	     {
	    	 setXY(x+1, y);
	     } 
	 }
	 public void moveLeft()
	 {
	     if(!(array[x][y-1]=='X'))
	     {
	        setXY(x, y-1);
	     }
	 }
	 public void moveRight()
	 {
	     if(!(array[x][y+1]=='X'))
	     {
	        setXY(x, y+1);
	     }
	 }


}
