package mb.home.geometricforms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale.FilteringMode;

public class FigureManager {
	
	private ArrayList<Figure> listFigure= new ArrayList<>();
	
	 public void add(Figure f)
	 {
		 listFigure.add(f);
	 }
	 public double getMaxPerimeter()
	 {
		 double max=listFigure.get(0).getPerimeter();
		 for (Figure figure : listFigure) 
		 {
			 if(figure.getPerimeter() > max)
			 {
			 	max = figure.getPerimeter();
			 }
		}
		 
		return max;

	 }
	 public double  getAverageAreaSize() {
		 double result=0.0;
		 for (Figure figure : listFigure) {
			 result += figure.getArea();	
		}
		 return result/listFigure.size();
	 }
	 
	 HashMap<String,Double> getAreaBySizeCategories()
	 {
		 HashMap<String, Double> result= new HashMap<>();
		 for (Figure figure : listFigure) 
		 {
			 if(result.containsKey("Klein") && figure.getArea() < 1000)
			 {
				 Double sum = figure.getArea();
				 sum = sum + figure.getArea();
				 result.put("Klein", sum);
			 }
				
			 if(!result.containsKey("Klein") && figure.getArea() < 1000)
			 {
				 result.put("Klein", figure.getArea());
			 }
			 
			 if(result.containsKey("Mittel") &&  figure.getArea() >= 1000 && figure.getArea() <= 4999)
			 {
				 Double sum = figure.getArea();
				 sum = sum + figure.getArea();
				 result.put("Mittel", sum);
			 }
				
			 if(!result.containsKey("Mittel") && figure.getArea() >= 1000 && figure.getArea() <= 4999)
			 {
				 result.put("Mittel", figure.getArea());
			 }
			 

			 if(result.containsKey("Gro�") && figure.getArea() >= 5000)
			 {
				 Double sum = figure.getArea();
				 sum = sum + figure.getArea();
				 result.put("Gro�", sum);
			 }
			 if(!result.containsKey("Gro�") && figure.getArea() >= 5000)
			 {
				 result.put("Gro�", figure.getArea());
			 }
		 }
		 return result;
	 }

}
