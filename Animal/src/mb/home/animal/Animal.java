/**
 * 
 */
package mb.home.animal;

/**
 * @author Maroua
 *
 */
public abstract class Animal {
	
	protected String color, name;
	 public Animal(String name,String color)
	 {
		 this.color=color;
		 this.name=name;
	 }
	 
	 public abstract void makeNoise();
	 public abstract String toString();

	public String getColor() {
		return color;
	}

	public abstract String getName();
	public abstract Integer getWeight();


}
