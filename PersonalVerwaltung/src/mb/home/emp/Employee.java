package mb.home.emp;

public class Employee {
	private String name, department;
	private double salary;
	private int empNumber;
	 public Employee(int empNumber, String name, double salary, String departement)
	 {
		 this.empNumber=empNumber;
		 this.name=name;
		 this.salary=salary;
		 this.department=departement;
	 }
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public String getName() {
		return name;
	}
	public String getDepartment() {
		return department;
	}
	public int getEmpNumber() {
		return empNumber;
	}
	@Override
	public String toString() {
		return "Employee [name=" + name + ", department=" + department + ", salary=" + salary + ", empNumber="
				+ empNumber + "]";
	}
	 
	 
}
