import java.util.Scanner;
import org.campus02.shape.FrameShape;

public class DemoSnake {

    public static void main(String[] args) 
    {
        FrameShape snake= new FrameShape();
        
        Scanner scan = null;
        try
        {
            scan= new Scanner(System.in);
            while(true)
            {
                int nbr= 0;
                while(nbr!=5 && nbr!=2 && nbr!=4 && nbr!=8 && nbr!=6)
                {
                    System.out.println("Press 5 to (re)start \n give a move code 2, 4, 6 or 8");
                    nbr = scan.nextInt();
                }
            
                switch (nbr) 
                {
                    //press 5 to start
                    case 5:
                        System.out.println(snake);
                        System.out.println(snake.getX());
                        System.out.println(snake.getY());
                        break;
                    case 2:
                        
                        snake.moveDown();
                        System.out.println(snake);
                        System.out.println(snake.getX());
                        System.out.println(snake.getY());
                        break;
                    case 4:
                        
                        snake.moveLeft();
                        System.out.println(snake);
                        System.out.println(snake.getX());
                        System.out.println(snake.getY());
                        break;
                    case 6:
                        
                        snake.moveRight();
                        System.out.println(snake);
                        System.out.println(snake.getX());
                        System.out.println(snake.getY());
                        break;
                    case 8:
                        snake.moveUp();
                        System.out.println(snake);    
                        System.out.println(snake.getX());
                        System.out.println(snake.getY());
                        break;
                    default:
                        break;
                }
            }
        }
        finally 
        {
            if (scan != null)
            {
                scan.close();
            }
        }
    }
}
