package mb.home.animal;

public class Box {

	protected int length;
	protected int width;
	protected int heigh;
	
	public Box(int length, int width,int heigh) {
		this.length=length;
		this.width=width;
		this.heigh=heigh;
	}

	@Override
	public String toString() {
		return "Box [length=" + length + ", width=" + width + ", heigh=" + heigh + "]";
	}
	
	public int calculVolume()
	{
		return this.heigh * this.length * this.width;
	}

}
