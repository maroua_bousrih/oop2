package mb.home.animal;

import java.util.ArrayList;
import java.util.HashMap;

public class DemoTransportBox {

	public static void main(String[] args) {
		Dog dog= new Dog("rafli", "black", 4000);
		Cat cat= new Cat("susi","white",true,1500);
		Frog frog = new Frog("fikhi", "green", "yellow",500);
		Dog dog1= new Dog("raffi", "black", 5000);
		Cat cat1= new Cat("ddf","white",true,2000);
		Frog frog1 = new Frog("fgtf", "green", "yellow",750);
		Dog dog2= new Dog("hipk", "black", 10000);
		Bird bird = new Bird("papagai","multicolor",750); 
		
		TransportBox<Animal> TB1= new TransportBox<>("red",100, 100,100,dog);
		TransportBox<Animal> TB2= new TransportBox<>("white",85, 100,100,cat);
		TransportBox<Animal> TB3= new TransportBox<>("green",50, 50,50,frog);
		TransportBox<Animal> TB4= new TransportBox<>("white",100, 100,100,dog1);
		TransportBox<Animal> TB5= new TransportBox<>("orange",100, 100,100,cat1);
		TransportBox<Animal> TB6= new TransportBox<>("pink",100, 100,100,frog1);
		TransportBox<Animal> TB7= new TransportBox<>("blue",100, 100,100,dog2);
		TransportBox<Animal> TB8= new TransportBox<>("red", 50, 50, 50, bird);
		
		
		ArrayList<TransportBox<Animal>> listBox= new ArrayList<>();
		listBox.add(TB1);
		listBox.add(TB2);
		listBox.add(TB3);
		listBox.add(TB4);
		listBox.add(TB5);
		listBox.add(TB6);
		listBox.add(TB7);
		listBox.add(TB8);
		
		for (TransportBox<Animal> transportBox : listBox) {
			System.out.println(transportBox + " content " +transportBox.getContent());
			
		}
		System.out.println("/////////////");
		
		System.out.println(countAnimalByType(listBox));
		
		
		
		
	}
	public static HashMap<String,Integer> countAnimalByType(ArrayList<TransportBox<Animal>> listBox)
	{
		HashMap<String, Integer> result= new HashMap<>();
		
		for (TransportBox<Animal> box : listBox) 
		{	
			if(result.containsKey(box.getContent().getClass().getSimpleName()))
			{
				
				int count= result.get(box.getContent().getClass().getSimpleName());
				count = count+1;
				result.put(box.getContent().getClass().getSimpleName(),count );
			}
			else
			{
				result.put(box.getContent().getClass().getSimpleName(), 1 );
			}
		}
		return result;
	}	
	
//	for (Animal animal : listAnimal) {
//		if(result.containsKey(animal.getColor()))
//		{
//			int count= result.get(animal.getColor());
//			count++;
//			result.put(animal.getColor(), count);
//		}
//		else
//		{
//			result.put(animal.getColor(), 1);
//		}
//		
//	}
//	
//	
//	
//	
}
