package logisticManager;

public class Shirt implements Moveable{

	private String brand,size,color;
	public Shirt(String brand, String size, String color) {
		this.brand=brand;
		this.size=size;
		this.color=color;
	}
	public String getBrand() {
		return brand;
	}
	public String getSize() {
		return size;
	}
	public String getColor() {
		return color;
	}
	@Override
	public String toString() {
		return "Shirt [brand=" + brand + ", size=" + size + ", color=" + color + "]";
	}
	@Override
	public void move(String destination) {
		System.out.println(color+" "+ brand +" " +size+" "+"move to " + destination);
		
	}

}
