package mb.home.animal;

import java.util.ArrayList;
import java.util.HashMap;

public class ZooManager {

	ArrayList<Animal> listAnimal= new ArrayList<Animal>();

	public void addAnimal(Animal animal)
	{
		listAnimal.add(animal);
	}

	@Override
	public String toString() {
		String result="Zoo Graz [";
		for (Animal animal : listAnimal) {
			result+= animal.toString()+" ";
		}
		result+="]";
		return result;
	}
	
	public HashMap<String, Integer> accountAnimalbyColor()
	{
		HashMap<String,Integer> result= new HashMap<String,Integer>();
		
		for (Animal animal : listAnimal) {
			if(result.containsKey(animal.getColor()))
			{
				int count= result.get(animal.getColor());
				count++;
				result.put(animal.getColor(), count);
			}
			else
			{
				result.put(animal.getColor(), 1);
			}
			
		}
		
		
		
		return result;
		
	}
	
	public HashMap<String, Integer> accountAnimalbyBreed()
	{
		HashMap<String,Integer> result= new HashMap<String,Integer>();
		
		for (Animal animal : listAnimal) {
			if(result.containsKey(animal.getClass().getSimpleName()))
			{
				int count= result.get(animal.getClass().getSimpleName());
				count++;
				result.put(animal.getClass().getSimpleName(), count);
			}
			else
			{
				result.put(animal.getClass().getSimpleName(), 1);
			}
			
		}
	
		return result;
		
	}
	public HashMap<String, Integer> sumweightbybyBreed()
	{
		HashMap<String, Integer> result= new HashMap<>();
		for (Animal animal : listAnimal) 
		{
			if(result.containsKey(animal.getClass().getSimpleName()))
			{
				int sum = result.get(animal.getClass().getSimpleName());
				sum = sum + animal.getWeight();
				result.put(animal.getClass().getSimpleName(), sum);
				
			}
			else
				result.put(animal.getClass().getSimpleName(), animal.getWeight());
			
		}
	
		return result;
		
	}
	
	public ArrayList<String> namebyAnimal(String animaltyp)
	{
		ArrayList<String> result=  new ArrayList<>();
		for (Animal animal : listAnimal) {
			if(animal.getClass().getSimpleName().equalsIgnoreCase(animaltyp))
			{
				result.add(animal.getName());
			}
			
		}
		return result;
	}
	
	public int  biggestAnimalByType(String  animaltyp)
	{
		int result=0;
		for (Animal animal : listAnimal) 
		{
			if(animal.getClass().getSimpleName().equalsIgnoreCase(animaltyp) && animal.getWeight()>result)
			{
				result = animal.getWeight();
			}	
		}
		return result;
	}
	
}
