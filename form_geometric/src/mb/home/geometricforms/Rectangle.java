package mb.home.geometricforms;

public class Rectangle implements Figure {
	private double length, width;

	public Rectangle(double length, double width) 
	{
		this.length=length;
		this.width=width;
	}

	@Override
	public double getPerimeter() {
		return this.length * this.width;
	}

	@Override
	public double getArea() {
		return this.length * this.width;
	}

	@Override
	public String toString() {
		return "Rectangle [length=" + length + ", width=" + width + "]";
	}
	

}
