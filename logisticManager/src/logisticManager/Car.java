package logisticManager;

public class Car implements Moveable{

	private String type;
	private String color;
	private int weight;
	public Car(String type, String color, int weight) {
		this.type=type;
		this.color=color;
		this.weight=weight;
		
	}
	public String getType() {
		return type;
	}
	public String getColor() {
		return color;
	}
	public int getWeight() {
		return weight;
	}
	@Override
	public String toString() {
		return "Car [type=" + type + ", color=" + color + ", weight=" + weight + "]";
	}
	@Override
	public void move(String destination) {
		System.out.println(color+" "+type +" move to " + destination);
		
	}
	
	

}
